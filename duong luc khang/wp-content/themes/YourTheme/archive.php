<?php get_header(); ?>
    <div id="wp_content">
        <div class="wp_inner">
            <div class="fl_left" style="width:  75%;">
                <div class="new_main">
                    <div class="new_left">
                        <?php
                        $cat_id = get_query_var('cat');
                        $args_left = new WP_Query('showposts=1&cat=' . $cat_id);
                        ?>
                        <?php
                        if ($args_left->have_posts()):
                            while ($args_left->have_posts()):
                                $args_left->the_post();
                                ?>
                                <div class="img_new_left">
                                    <div class=thumb_cover>
                                        <img width="400" height="300" <?php the_post_thumbnail(); ?>
                                        <div class="info_post">
                                            <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>

                                            <div class="desc"><?php the_excerpt(); ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            endwhile;
                        endif; ?>
                    </div>
                    <div class="new_right">
                        <?php
                        $cat_id = get_query_var('cat');
                        $args_right = new WP_Query('offset=1&showposts=3&cat=' . $cat_id);
                        ?>
                        <?php
                        if ($args_right->have_posts()):
                            while ($args_right->have_posts()):
                                $args_right->the_post();
                                ?>
                                <div class="right clearfix">
                                    <div class="thumb_right">
                                        <img width="400" height="267" <?php the_post_thumbnail(); ?>
                                    </div>
                                    <a href="<?php the_permalink();?>">
                                        <h5><?php the_title();?></h5>
                                    </a>

                                    <div class="desc">
                                        <?php the_content_rss('', TRUE, '', 20); ?>
                                    </div>
                                </div>
                            <?php
                            endwhile;
                        endif; ?>
                    </div>
                </div>
                <div class="new_bottom">
                    <?php
                    $cat_id = get_query_var('cat');
                    $args_bottom = new WP_Query('offset=4&showposts=3&cat=' . $cat_id);
                    ?>
                    <?php
                    if ($args_bottom->have_posts()):
                        while ($args_bottom->have_posts()):
                            $args_bottom->the_post();
                            ?>
                            <div class="items_bottom">
                                <div class="thumb_bottom">
                                    <img width="400" height="267"><?php the_post_thumbnail();?>
                                </div>
                                <a href="<?php the_permalink();?>"><h4><?php the_title();?></h4></a>

                                <div class="desc"><p><?php the_excerpt(); ?></p></div>
                            </div>
                        <?php
                        endwhile;
                    endif; ?>
                </div>
            </div>
            <div class="sidebar">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>


<?php get_footer(); ?>