<?php get_header(); ?>
    <div id="wrapper_file">
        <div class="wp_inner">
            <div class="content_file">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <div class="post" id="post-<?php the_ID(); ?>">
                        <?php ?>

                        <h2><?php the_title(); ?></h2>

                        <div class="detail">

                            <?php the_content(); ?>

                            <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                        </div>
                    </div>
                <?php
                endwhile;
                endif;
                ?>
            </div>
            <div id="sidebar">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>