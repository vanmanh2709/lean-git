<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>

    <?php if (is_search()) { ?>
        <meta name="robots" content="noindex, nofollow"/>
    <?php } ?>

    <title>
        <?php
        if (function_exists('is_tag') && is_tag()) {
            single_tag_title("Tag Archive for &quot;");
            echo '&quot; - ';
        } elseif (is_archive()) {
            wp_title('');
            echo ' Archive - ';
        } elseif (is_search()) {
            echo 'Search for &quot;' . wp_specialchars($s) . '&quot; - ';
        } elseif (!(is_404()) && (is_single()) || (is_page())) {
            wp_title('');
            echo ' - ';
        } elseif (is_404()) {
            echo 'Not Found - ';
        }
        if (is_home()) {
            bloginfo('name');
            echo ' - ';
            bloginfo('description');
        } else {
            bloginfo('name');
        }
        if ($paged > 1) {
            echo ' - page ' . $paged;
        }
        ?>
    </title>

    <link rel="shortcut icon" href="/favicon.ico">


    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>

    <?php wp_head(); ?>
    <link href="<?php bloginfo('template_directory'); ?>/css/reset.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link href="<?php bloginfo('template_directory'); ?>/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="<?php bloginfo('template_directory'); ?>css/owl.theme.css" rel="stylesheet" type="text/css"/>
    <link href="<?php bloginfo('template_directory'); ?>/css/fonts/awesome/font-awesome.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php bloginfo('template_directory'); ?>/css/fonts.css" rel="stylesheet" type="text/css"/>

    <script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/main.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_directory');?>/js/tuvan.js" type="text/javascript"></script>

</head>

<body <?php body_class(); ?>>

<div id="page-wrap">

    <div id="head">
        <div class="wp_inner">
            <div class="title">
                <h1>dương lực khang</h1>
            </div>
            <div id="lienhe" class="clearfix">
                <ul id="icon">
                    <li class="thumb">
                        <a href="">
                            <img src="<?php bloginfo('template_directory'); ?>/images/icon-f.png" alt=""/>
                        </a>
                    </li>
                    <li class="thumb fl_left">
                        <a href="">
                            <img src="<?php bloginfo('template_directory'); ?>/images/icon-t.png" alt=""/>
                        </a>
                    </li>
                    <li class="thumb fl_right">
                        <a href="">
                            <img src="<?php bloginfo('template_directory'); ?>/images/icon-g.png" alt=""/>
                        </a>
                    </li>
                </ul>
                <div id="contact">
                    <div class="hotline fl_left">
                        <div class="icon-dt fl_left">
                            <img src="<?php bloginfo('template_directory'); ?>/images/icon-dienthoai.png" alt=""/>
                        </div>
                        <span> 0965.209.877/0983.454.461</span>
                    </div>
                    <div class="email fl_right">
                        <div class="icon-dt fl_left">
                            <img src="<?php bloginfo('template_directory'); ?>/images/icon-hopthu.png" alt=""/>
                        </div>
                        <span>Tuvanduongluckhang@gmail.com</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu-page-menu" class="clearfix">
            <div class="wp_inner">
                <?php wp_nav_menu(array('menu' => 'Page Menu', 'container' => '')); ?>

            </div>

        </div>
        <div class="thumb clearfix">
            <div id="slide" class="clearfix">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item">
                        <img src="<?php bloginfo('template_directory'); ?>/images/bg.png" alt=""/>
                    </div>
                    <div class="item">
                        <img src="<?php bloginfo('template_directory'); ?>/images/slide.jpg" alt=""/>
                    </div>
                    <div class="item">
                        <img src="<?php bloginfo('template_directory'); ?>/images/slede2.jpg" alt=""/>
                    </div>
                </div>
            </div>
            <!-- end#slide -->
        </div>

    </div>
</div>