<?php get_header(); ?>

    <div id="block_home" xmlns="http://www.w3.org/1999/html">
        <div class="wp_inner">
            <div class="block_content_left">
                <h2  style="margin-top: 30px; font-size: 16px;color: #FFFFFF;">Chuyên gia tư vấn </h2>
                <div class="line-link-cat"></div>
                <div class="panel_group">
                    <?php
                    $cat_it = get_query_var('cat');
                    $args_ = new  WP_Query('posts_per_page=5&cat=' . $cat_it);
                    ?>
                    <?php
                    if ($args_->have_posts()):
                        while ($args_->have_posts()):
                            $args_->the_post();
                            ?>
                            <dl class="list_info_us">
                                <dt class="active"><i class="fa fa-minus"></i><p><?php the_title(); ?></p></dt>
                                <dd class="detail_show">
                                    <?php the_content(); ?>
                                </dd>
                            </dl>
                        <?php
                        endwhile;
                    endif; ?>
                </div>
                <div class="contact_form7">
                   <h2>Gửi câu hỏi</h2>
                    <?php echo do_shortcode('[contact-form-7 id="193" title="Gửi câu hỏi"]' )?>
                </div>

            </div>
            <div class="sidebar">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>