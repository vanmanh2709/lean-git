<?php get_header(); ?>
 <div id="home_contact">
     <div class="wp_inner">
         <div class="block_contact">
             <div class="contact">
                 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                     <div class="post" id="post-<?php the_ID(); ?>">
                         <?php ?>

                         <h2><?php the_title(); ?></h2>

                         <div class="detail">

                             <?php the_content(); ?>

                             <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                         </div>
                     </div>
                 <?php
                 endwhile;
                 endif;
                 ?>
             </div>
             <div class="idea">
                 <h2>Ý kiến bạn đọc</h2>
                 <?php echo do_shortcode('[contact-form-7 id="196" title="Ý kiến bạn đọc"]') ?>
             </div>
             <div class="map">
                 <h2>Bản đồ</h2>
                <?php echo do_shortcode(' [wpgmza id="1"]') ?>
             </div>
         </div>
     <div id="sidebar">
         <?php get_sidebar(); ?>
     </div>
     </div>
 </div>
<?php  get_footer();?>