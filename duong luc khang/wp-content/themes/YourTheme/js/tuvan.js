/**
 * Created by Manh on 10/12/2015.
 */

$(document).ready(function(){
    $('.list_info_us dt').before('<i class="fa fa-plus"></i>');
    list_info_us();
});
function list_info_us() {
    $('.list_info_us dd').hide();
    $('.list_info_us dd:first').show();
    $('.list_info_us dt:first').addClass('active');
    $('.list_info_us dt:first i').removeClass('fa-plug').addClass('fa-minus');
    $('.list_info_us dt').click(function () {
        if ($(this).next().css('display') == 'none') {
            $('.list_info_us dt').removeClass('active');
            $(this).addClass('active');
            $('.list_info_us i').removeClass('fa-minus').addClass('fa-plus');
            $('.list_info_us dd').slideUp('normal');
            $(this).next().slideDown();
            $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
        }
    });
}